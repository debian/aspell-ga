aspell-ga (0.50-4-6) unstable; urgency=medium

  * QA upload.
  * Bump debhelper to 13:
    - Update debhelper-compat version in Build-Depends
    - Drop unused d/dirs
    - Add a d/clean to remove generated Makefile
  * Use modern dictionaries-common-dev helpers:
    - Replace dictionaries-common-dev by dh-sequence-aspell-simple
    - Replace aspell and dictionaries-common by ${aspell:Depends} in Depends
    - Replace manual aspell logic by aspell-simple helper in d/rules
  * Update packaging project on Salsa:
    - Add Salsa CI pipeline
    - Use recommended branch name from DEP14
  * Add watch file and upstream current signing key
  * Update copyright:
    - Add myself to copyright on debian/*
    - Use https scheme in the Source field
  * Bump Standard-Version to 4.5.1
  * Add Homepage field
  * Set Rules-Requires-Root: no
  * Add lintian-overrides to silence false positives:
    - Homepage is not a directory listing
    - Text is the correct section for an aspell dictionary

 -- Baptiste Beauplat <lyknode@cilg.org>  Wed, 24 Feb 2021 20:26:03 +0100

aspell-ga (0.50-4-5) unstable; urgency=medium

  * QA upload.
  * Mark source format as 3.0
  * Update Standards-Version to 4.5.0
  * Orphan the package. (See: #915443)
  * Update compat level to 12
    - Add dependency on ${misc:Depends}.
  * Remove dependency on cdbs.
  * Add build dependency on aspell.
  * Link copyright to GPL-2
  * Update FSF address.
  * Point Vcs to salsa.
  * Update d/copyright to GPL-2.0+ according to upstream.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Sat, 21 Mar 2020 20:13:55 +0000

aspell-ga (0.50-4-4.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Exclude /var/lib/aspell from dh_md5sums

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sat, 17 Dec 2016 22:19:09 +0100

aspell-ga (0.50-4-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use debhelper v10. Closes: #817357

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 16 Dec 2016 22:46:18 +0100

aspell-ga (0.50-4-4) unstable; urgency=low

  * Converted to use aspell-autobuildhash to build the dictionary hashes
    at install time, thus allowing the package to become Arch: all.
  * debian/control: removed the build-dependency on aspell since it's no
    longer needed
  * Added support for dictionary registration in dictionaries-common

 -- Brian Nelson <pyro@debian.org>  Thu, 21 Jul 2005 19:08:31 -0700

aspell-ga (0.50-4-3) unstable; urgency=low

  * Added a versioned dependency on libaspell15 (>> 0.60), since this
    dictionary will not work with earlier libaspell15 versions.

 -- Brian Nelson <pyro@debian.org>  Tue, 22 Feb 2005 11:02:15 -0800

aspell-ga (0.50-4-2) unstable; urgency=low

  * Support Aspell 0.60
    - debian/control: build-depend on aspell-bin (>> 0.60)
    - debian/control: provide aspell6-dictionary

 -- Brian Nelson <pyro@debian.org>  Fri, 21 Jan 2005 10:45:23 -0800

aspell-ga (0.50-4-1) unstable; urgency=low

  * Initial release (Closes: #237736)

 -- Brian Nelson <pyro@debian.org>  Fri, 12 Mar 2004 22:06:09 -0800
